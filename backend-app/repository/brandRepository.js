const _ = require('lodash');

const Brands = [
    "Brand 1",
    "Brand 2",
    "Brand 3",
    "Brand 4",
    "Brand 5",
    "Brand 6",
    "Brand 7",
    "Brand 8",
    "Brand 9",
    "Brand 10",
    "Brand 11",
    "Brand 12",
    "Brand 13",
    "Brand 14",
    "Brand 15",
    "Brand 16",
    "Brand 17"
];

const PurchasedBrands = [];

class BrandRepository {
    static getBrands() {
        return Brands;
    }

    static savePurchasedBrands(brands) {
        PurchasedBrands.push(brands);
    }

    static getPurchasedBrands() {
        return PurchasedBrands;
    }

    static hasPurchasedBrandGroup(brands) {

        for(let index in PurchasedBrands) {
            if(_.isEqual(PurchasedBrands[index], brands)) {
                return true;
            }
        }
        return false;
    }
}

module.exports = BrandRepository;
