const brandRepository = require('../repository/brandRepository');

class BrandService {
    static getBrands() {
        return brandRepository.getBrands();
    }

    static savePurchasedBrands(brands) {
        let brandsSorted = brands.sort();

        if(brandRepository.hasPurchasedBrandGroup(brandsSorted)) {
            return {
                errorMessage: "This purchase group already exists.",
                success: false
            }
        }
        else {
            brandRepository.savePurchasedBrands(brandsSorted);
            return {
                success: true
            };
        }
    }

    static getPurchasedBrands() {
        return brandRepository.getPurchasedBrands()
    }
}

module.exports = BrandService;