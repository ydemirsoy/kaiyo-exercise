const brandService = require('../services/brandService');
const ResponseModel = require('../model/responseModel');

module.exports = async function (app, opts) {
    app.get('/', (req, reply) => {
        let brands = brandService.getBrands();
        reply.send(brands);
    });

    app.post('/purchased', (req, reply) => {
        brands = req.body.brands;
        let result = brandService.savePurchasedBrands(brands);

        if(!result.success) {
            return new ResponseModel(result.errorMessage, result.success)
        }

        reply.code(201).send(new ResponseModel("Successfully created", true, brands));
    });

    app.get('/purchased', (req, reply) => {

        let purchasedBrands = brandService.getPurchasedBrands();
        reply.send(purchasedBrands);
    });
};

module.exports.autoPrefix = '/brand';

