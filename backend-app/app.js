const path = require('path');
const fastify = require('fastify');
const autoLoad = require('fastify-autoload');
const fastifyCORS = require('fastify-cors');

const app = fastify({ logger: true });

app.register(fastifyCORS, { origin: '*' });
app.register(autoLoad, {
    dir: path.join(__dirname, 'routes')
});

const start = async () => {
    try {
        await app.listen(3001, "0.0.0.0");
    } catch (err) {
        app.log.error(err);
        process.exit(1);
    }
};

start();