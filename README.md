# Kaiyo Exercise

This exercise contains one frontend and one backend applications.

### Technologies Used
Frontend App: React.js

Backend App: Node.js


## Installation & Run

###Option 1 - Use Docker:
If docker is installed on your computer, you can follow this option.

Extracted folder contains a `docker-compose.yml` file. Go to the directory that contains the file and run following command:
```bash
docker-compose up -d
```

This command will start running backend and frontend apps in two different containers and you can react apps from following ports:

Frontend: http://localhost:3000

Backend: http://localhost:3001

Use following command to stop containers:
```
docker-compose down
```

###Option 2 - Run Manually:

Node.js must be installed on the computer.

Extracted folder contains a `build.sh` file. Open a terminal window and run following command to install dependencies.

```
./build.sh
```

To run the backend app, go to the backend app directory and run following command:
```
npm start
```

To run the frontend app, go to the frontend app directory  and run following command:
```
npm start
```

##Backend Endpoints

Backend app contains three endpoints:

1) Endpoint to return all of the brands

    GET http://localhost:3001/brand 

2) Endpoint to return purchased brand groups

    GET http://localhost:3001/brand/purchased 

3) Endpoint to save purchased brand groups

    POST http://localhost:3001/brand/purchased
    
    Payload:
    ```
   {
       "brands": ["Brand 1", "Brand 2", "Brand 3",  "Brand 4",  "Brand 5",  "Brand 6"]
   }
   ```
    
