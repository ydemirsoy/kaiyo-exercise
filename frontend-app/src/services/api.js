import axios from "axios";

const baseUrl = 'http://localhost:3001';

const config = {
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
    }
};

class Api {
    static getBrands() {
        return axios.get(baseUrl + '/brand', config).then((res) => {
            return res.data;
        });
    }

    static getPurchasedBrands() {
        return axios.get(baseUrl + '/brand/purchased', config).then((res) => {
            return res.data;
        });
    }

    static savePurchasedBrands(brands) {
        return axios.post(baseUrl + '/brand/purchased', { brands: brands }, config)
            .then((res) => {
                return res.data;
            });
    }
}

export default Api