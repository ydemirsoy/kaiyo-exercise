import React, {Component} from "react";
import {ListViewComponent} from '@syncfusion/ej2-react-lists';
import './App.css';
import api from "./services/api";
import "../node_modules/@syncfusion/ej2-base/styles/material.css";
import "../node_modules/@syncfusion/ej2-react-lists/styles/material.css";
import "../node_modules/@syncfusion/ej2-buttons/styles/material.css";
import Notifications, {notify} from 'react-notify-toast';

class App extends Component {

    constructor(props) {
        super(props);

        this.brandListObj = null;
        this.state = { brands: [], purchasedBrands: [], selectedBrands: []};

        Promise.all([api.getBrands(), api.getPurchasedBrands()]).then((data) => {
           this.setState({brands: data[0], purchasedBrands: this.getPurchasedBrandsConcat(data[1]) });
        });

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
       api.savePurchasedBrands(this.brandListObj.getSelectedItems().text).then((data) => {
           notify.hide();
           if(data.success) {
               api.getPurchasedBrands().then((data) => {
                   this.setState({
                       purchasedBrands: this.getPurchasedBrandsConcat(data)
                   });
                   notify.show('Brand group added successfully!', 'success');
               })
           } else {
               notify.show('This brand group is already added!', 'error');
           }
        })
    }

    getPurchasedBrandsConcat(l) {
        let result = [];
        for(let i in l) {
            result.push(l[i].join(", "));
        }
        return result;
    }

    render() {
        return (
            <div className="pageWrapper">
                <h1>Kaiyo Exercise</h1>
                <div className="listWrapper left">
                    <ListViewComponent id='listBrands' dataSource={this.state.brands} showCheckBox={true}
                                       headerTitle='Brands' showHeader={true} ref={scope => { this.brandListObj = scope; }}/>
                </div>
                <button className="button left" onClick={this.handleClick}>Add To Purchased</button>
                <div className="listWrapper left">
                    <ListViewComponent id='listPurchased' dataSource={this.state.purchasedBrands}
                                       headerTitle='Purchased Brands' showHeader={true}/>
                </div>
                <div>
                    <Notifications/>
                </div>
            </div>
        );
    }
}

export default App;
